import React, { Component } from 'react'
import { Button, Modal } from 'react-bootstrap'

export default class ItemModalCard extends Component {
    render() {
        return (
            <div>
                <Modal show={this.props.show}  onHide={this.props.close}>        
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.persons.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <p>Gender: {this.props.persons.gender}</p>
                       <div>
                           <ul>
                            <li style={this.props.persons.job1===true?{display:""}:{display:"none"}}>Student</li>
                            <li style={this.props.persons.job2===true?{display:""}:{display:"none"}}>Teacher</li>
                            <li style={this.props.persons.job3===true?{display:""}:{display:"none"}}>Developer</li>
                           </ul>
                       </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.props.close} >Close</Button>                      
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}