import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card,DropdownButton,Dropdown} from 'react-bootstrap'
import ItemModal from './ItemModalCard';
import Moment from 'react-moment';
import 'moment/locale/km'
export default class ItemCard extends Component {
    constructor(props){
        super(props);
        this.state={
            show:false,   
        }
    }
    show(){
      this.setState({show:true})
    }
    close(){
        this.setState({show:false})
    }
    render() {
      var minute=new Date(this.props.persons.time);
        return (
            <Card style={{height:"100%"}}>
                <Card.Header className="text-center">
                    <DropdownButton id="dropdown-basic-button" title="Action">
                        <Dropdown.Item href="#/action-1" onClick={this.show.bind(this)}>View</Dropdown.Item>
                        <ItemModal show={this.state.show} close={this.close.bind(this)} persons={this.props.persons}></ItemModal>
                        <Dropdown.Item href="#/action-2" onClick={()=>this.props.onDelete(this.props.id)}>Delete</Dropdown.Item>
                    </DropdownButton>
                </Card.Header>
                <Card.Body>
                    <h3>{this.props.persons.name}</h3>
                    <h5>Jobs:</h5>
                    <ul>
                    <li style={this.props.persons.job1===true?{display:""}:{display:"none"}}>Student</li>
                    <li style={this.props.persons.job2===true?{display:""}:{display:"none"}}>Teacher</li>
                    <li style={this.props.persons.job3===true?{display:""}:{display:"none"}}>Developer</li>
                    </ul>
                
                </Card.Body>
                <Card.Footer className="text-muted text-center"><Moment fromNow>{minute}</Moment></Card.Footer>
            </Card>   
        )
    }
}
