import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Main from './Component/Main'
class App extends Component {
  render() { 
    return (
      <div className="row">
         <Main/>
      </div>
    )
  }
}
ReactDOM.render(<App />, document.getElementById('root'));