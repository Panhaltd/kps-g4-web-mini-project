import React, { Component } from "react";
import { Button } from "react-bootstrap";
import Moment from "react-moment";
import "moment/locale/km";
import ItemModalList from "./ItemModalList";
export default class ListData extends Component {
  constructor(props){
    super(props);
    this.state={
      show:false,
    }
  }
  viewItem() {
    this.setState({show:true})
  }
  close(){
    this.setState({show:false})
  }
  render() {

    return (
      <tr>
          <td>{this.props.persons.id}</td>
          <td>{this.props.persons.name}</td>
          <td>{this.props.persons.age}</td>
          <td>{this.props.persons.gender}</td>
          <td>
            <ul>
            <li style={this.props.persons.job1===true?{display:""}:{display:"none"}}>Student</li>
            <li style={this.props.persons.job2===true?{display:""}:{display:"none"}}>Teacher</li>
            <li style={this.props.persons.job3===true?{display:""}:{display:"none"}}>Developer</li>
            </ul>
            </td>
          <td><Moment fromNow locale="km">{this.props.persons.time}</Moment></td>
          <td><Moment fromNow locale="km">{this.props.persons.upDateTime}</Moment></td>
          <td>
            <ItemModalList show={this.state.show} close={this.close.bind(this)} persons={this.props.persons}/>
            <Button variant="info" onClick={this.viewItem.bind(this)}> View </Button>{" "}
            <Button variant="warning" onClick={()=>this.props.onUpPeople(this.props.persons.index,this.props.persons)}>Update</Button>{" "}
            <Button variant="danger" onClick={()=>this.props.onDelete(this.props.id)}> Delete</Button>
          </td>
      </tr>    
    );
  }
}
