import React, { Component } from 'react'
import Moment from 'react-moment'
import 'moment/locale/km'

export default class testdata extends Component {
    render() {

        var item = this.props.item.map((it,value)=>
            <tr key={value}>
                <td>{it.id}</td>
                <td>{it.name}</td>
                <td>{it.age}</td>
                <td>{it.gender}</td>
                <td>{it.job}</td>
                <td><Moment fromNow locale="km">{it.time}</Moment></td>
            </tr>
        )

        return (
            <div>
                {item}
                {/* <table>
                    {item}
                </table> */}
            </div>
        )
    }
}
