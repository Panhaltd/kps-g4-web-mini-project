import React, { Component } from "react";
import { Button, Table } from "react-bootstrap";
import Pagination from "react-js-pagination";

export default class ListPagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: 1,
    };
  }

  handlePageChange(pageNumber) {
    this.setState({ activePage: pageNumber });
  }

  render() {
    let indexOfLastTodo = this.state.activePage * 3;
    let indexOfFirstTodo = indexOfLastTodo - 3;
    let currentTodos = this.props.item.slice(indexOfFirstTodo, indexOfLastTodo);
    console.log(currentTodos);
    let list = currentTodos.map((people, index) => (
      <tr key={index}>
        <td>{people.id}</td>
        <td>{people.name}</td>
        <td>{people.age}</td>
        <td>{people.gender}</td>
        <td>{people.job}</td>
        <td>{people.create_at}</td>
        <td>{people.update_at}</td>
        <td>
          <Button className="btn btn-info ml-2">View</Button>
          <Button className="btn btn-warning ml-2">Update</Button>
          <Button className="btn btn-danger ml-2">Delete</Button>
        </td>
      </tr>
    ));
    return (
      <div>
        {" "}
        <div className="mt-4">
          <Table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Age</th>
                <th>Gender</th>
                <th>Job</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>{list}</tbody>
          </Table>
          <Pagination
            prevPageText="prev"
            nextPageText="next"
            activePage={this.state.activePage}
            itemsCountPerPage={3}
            totalItemsCount={this.props.item.length}
            linkClass={"page-link"}
            itemClass={"page-item"}
            // pageRangeDisplayed={10}
            onChange={this.handlePageChange.bind(this)}
          />
        </div>
      </div>
    );
  }
}
