import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './Main.css'
// import Next from './allfunction/next';
import 'react-moment'
import ListData from '../Component/allfunction/ListData';
import {Table} from 'react-bootstrap'
import ItemCard from '../Components/ItemCard';
import Pagination from 'react-js-pagination'



export default class Main extends Component {

    constructor() {
        super();
        this.state={
            index:0,
            checkName:true,
            checkAge:true,
            activePage: 1,
            listCard:'list',
            name:'',
            age:'',
            gender:'Male',
            genderRequire:'',
            nameRequire:'',
            ageRequire:'',
            jobRequire:'',
            job1:false,
            job2:false,
            job3:false,
            jobItem:[],
            item:[],
            time:'',
            isUpdate:false,
            updateIndex:null,
        }
        this.submit=this.submit.bind(this)
        this.changeToList=this.changeToList.bind(this);
        this.changeToCard=this.changeToCard.bind(this);
    }

    handleRadioButton=(event)=>{
        const target = event.target;
        var value = target.value;

        if(target.checked){
            this.setState({
                gender:value
            })
        }
    }

    handleCheckBox=(job)=>{
        var jobCheck = job.target;
        var isTick = jobCheck.checked;
        
        var name = jobCheck.name;
        if(jobCheck.type === "checkbox")
        {
            if(jobCheck.checked === true)
            {
                isTick = jobCheck.checked
                this.setState({
                    job: {...this.state.job, [name]: isTick},
                    checkJob: isTick,
                    errorJob: ''
                })   
            }    
            else{
                this.setState({
                    job:{...this.state.job, [name]: isTick}
                    ,      
                }) 
            }       
        }
    }

    handleName=(event)=>{
        const target = event.target;
        var value = target.value;
        
        this.setState({
            name:value
        })
    }

    handleAge=(event)=>{
        const target = event.target;
        var value = target.value;
        this.setState({
            age:value
        })
    }

    makeid(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    submit(){
        var pattern = /[^0-9]/gi;
        var pattern2 = /[^a-z\s]/gi;
        if(this.state.name.match(pattern2)!=null||this.state.name===""){
            this.setState({
                nameRequire:"Invalid Input or Input missing!"
            })
        }
        else if(this.state.age.match(pattern)!=null||this.state.age===""){
            this.setState({
                nameRequire:"",
                ageRequire:"Invalid Input or Input missing!"
            })
        }
        else if(this.state.gender===''){
            this.setState({
                nameRequire:"",
                ageRequire:"",
                genderRequire:"Please Choose One!"
            })
        }
        else if(this.state.job1===false&&this.state.job2===false&&this.state.job3===false){
            this.setState({
                nameRequire:"",
                ageRequire:"",
                genderRequire:"",
                jobRequire:"Please Choose One!"
            })
        }
        else{
            if(this.state.isUpdate){
                var item = [...this.state.item]
                item[this.state.updateIndex].name=this.state.name
                item[this.state.updateIndex].age=this.state.age
                item[this.state.updateIndex].gender=this.state.gender
                item[this.state.updateIndex].job1=this.state.job1
                item[this.state.updateIndex].job2=this.state.job2
                item[this.state.updateIndex].job3=this.state.job3
                item[this.state.updateIndex].upDateTime=new Date().toISOString()
                console.log(item[this.state.updateIndex].name)
                this.setState({
                    nameRequire:"",
                    ageRequire:"",
                    genderRequire:"",
                    jobRequire:"",
                    item,
                    isUpdate:false,
                    name:"",
                    age:"",
                    gender:"Male",
                    job1:false,
                    job2:false,
                    job3:false,
                })
            }else{
                console.log(this.state.index);
            var item1={
                index:this.state.index,
                id:this.makeid(5),
                name:this.state.name,
                age:this.state.age,
                gender:this.state.gender,
                job1:this.state.job1,
                job2:this.state.job2,
                job3:this.state.job3,
                time:new Date().toISOString(),
                upDateTime:new Date().toISOString()
            }
            this.setState((prevState,props)=>{
                return{index:prevState.index+1}
            })
            this.setState({
                item:[...this.state.item,item1],
                nameRequire:"",
                ageRequire:"",
                genderRequire:"",
                jobRequire:"",
                name:'',
                age:'',
                gender:'Male',
                job1:false,
                job2:false,
                job3:false
            })}
        }
    }

    changeToList(){
        this.setState({
            listCard:"list"
        })
    }

    changeToCard(){
        this.setState({
            listCard:"card"
        })
    }

    handleJob1=()=>{
        this.setState({
            job1:!this.state.job1
        })
    }

    handleJob2=()=>{
        this.setState({
            job2:!this.state.job2
        })
    }

    handleJob3=()=>{
        this.setState({
            job3:!this.state.job3
        })
    }

    handleDelete = itemId => {
        const items = this.state.item.filter(item => item.id !== itemId);
        this.setState({ item: items });
    };

    removePeople = (e) => {
        this.setState({
          item: [...this.state.item.filter((people) => people.id !== e)],
        });
        console.log(this.state.item)
    };

    upPeople = (index,person) => {

        let indexItem = this.state.item.findIndex((p)=>p.index===person.index)
        
        this.setState({
            updateIndex:indexItem,
            name:person.name,
            age:person.age,
            gender:person.gender,
            job1:person.job1,
            job2:person.job2,
            job3:person.job3,
            isUpdate:true,
        })
    };

    handlePageChange(pageNumber){
        this.setState({ activePage: pageNumber });
    }

    render() {
        
        let indexOfLastTodo = this.state.activePage * 3;
        let indexOfFirstTodo = indexOfLastTodo - 3;
        let currentTodos = this.state.item.slice(indexOfFirstTodo, indexOfLastTodo);
        var ListItem=currentTodos.map((value,index)=><ListData key={index} persons={value} id={value.id} onDelete={this.removePeople} onUpPeople={this.upPeople}/>)

        return (
            <div className="container">               
                <h1>Personal Information</h1>
                <div className="row">
                    <div className="col-md-8 left-input">
                        <h6>Name : <span style={{color:'red'}}>{this.state.nameRequire}</span></h6>
                        <input
                            id="name"
                            value={this.state.name}
                            onChange={this.handleName}
                            placeholder="Input Name" />
                        <h6>Age : <span style={{color:'red'}}>{this.state.ageRequire}</span></h6>
                        <input
                            id="age" 
                            onChange={this.handleAge}
                            value={this.state.age}
                            placeholder="Age"/>
                        <button style={this.state.isUpdate?{backgroundColor:"#ffd31d", color:"#000",borderColor:"#000"}:{backgroundColor:"#28a745",color:"#fff",borderColor:"#000"}} onClick={()=>this.submit()}>{this.state.isUpdate?"Update":"Submit"}</button>
                    </div>
                    <div className="col-md-4 right-input">
                        <h4>Gender : <span style={{color:'red'}}>{this.state.genderRequire}</span></h4>
                        <input 
                            id="id"
                            onChange={this.handleRadioButton}
                            type="radio" 
                            name="gender" 
                            checked={this.state.gender==='Male'}
                            value="Male"/>
                        <label>Male</label>
                        <input 
                            onChange={this.handleRadioButton}
                            style={{marginLeft:15}} 
                            type="radio" 
                            name="gender" 
                            checked={this.state.gender==='Female'}
                            value="Female"/>
                        <label>Female</label>
                        <h4>Job : <span style={{color:'red'}}>{this.state.jobRequire}</span></h4>
                        <input
                            onChange={this.handleJob1}
                            type="checkbox" 
                            name="Student"
                            checked={this.state.job1}
                            value="Student"/>
                        <label>Student</label>
                        <input
                            onChange={this.handleJob2}
                            style={{marginLeft:15}} 
                            type="checkbox" 
                            name="Teacher" 
                            checked={this.state.job2}
                            value="Teacher"/>
                        <label>Teacher</label>
                        <input
                            onChange={this.handleJob3}
                            style={{marginLeft:15}} 
                            type="checkbox" 
                            name="Developer" 
                            checked={this.state.job3}
                            value="Developer"/>
                        <label>Developer</label>
                    </div>
                </div>
                <div className="row">
                    <div className="display-data mb-3">
                        <center>
                        <h5>Display data : </h5>
                        <button style={{backgroundColor:"#17a2b8",color:"#fff"}} onClick={()=>this.changeToList()}>List</button>
                        <button style={{backgroundColor:"#dc3545",color:"#fff"}} onClick={()=>this.changeToCard()}>Card</button>
                        {/* #dc3545 */}
                        </center>
                    </div>
                </div>
                <div className="row">
                    {this.state.listCard==="list"?(
                        <div className="tableList"><Table>
                        <thead>
                            <tr className="bg-secondary text-center  text-white">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Jobs</th>
                            <th>Create At</th>
                            <th>Update At</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody className="text-center">
                            {/* call listData as ListItem we pass above */}
                            {ListItem}
                            </tbody>    
                        </Table>
                        <div className="divPage">
                        <Pagination
                        className="pagegin"
                        prevPageText="prev"
                        nextPageText="next"
                        activePage={this.state.activePage}
                        itemsCountPerPage={3}
                        totalItemsCount={this.state.item.length}
                        linkClass={"page-link"}
                        itemClass={"page-item"}
                        onChange={this.handlePageChange.bind(this)}
                        />
                        </div></div>
                    ):(
                        this.state.item.map((value,index)=> <div className="col-3 mt-3" key={index}><ItemCard id={value.id} persons={value} onDelete={this.handleDelete}/></div>)
                    )}
                </div>
            </div>
        )
    }
}
